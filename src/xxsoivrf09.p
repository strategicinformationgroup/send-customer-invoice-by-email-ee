/*DC45* xxsoivrf09.p -  SEND INVOICES IN PDF FORMAT BY EMAIL I          */
/* soivrp09.p - INVOICE HISTORY REPORT BY INVOICE                       */
/* Copyright 1986 QAD Inc. All rights reserved.                               */
/* $Id:: soivrp09.p 61132 2015-10-20 06:05:53Z ipa                         $: */
/* REVISION: 5.0      LAST MODIFIED: 01/09/90   BY: ftb *B511**/
/* REVISION: 6.0      LAST MODIFIED: 04/13/90   BY: ftb *D002**/
/* REVISION: 6.0      LAST MODIFIED: 09/18/90   BY: MLB *D055**/
/* REVISION: 6.0      LAST MODIFIED: 11/12/90   BY: MLB *D202**/
/* REVISION: 6.0      LAST MODIFIED: 03/28/91   BY: afs *D464**/
/* REVISION: 6.0      LAST MODIFIED: 04/05/91   BY: bjb *D507**/
/* REVISION: 6.0      LAST MODIFIED: 04/10/91   BY: MLV *D506**/
/* REVISION: 6.0      LAST MODIFIED: 05/07/91   BY: MLV *D617**/
/* REVISION: 6.0      LAST MODIFIED: 08/14/91   BY: MLV *D825**/
/* REVISION: 6.0      LAST MODIFIED: 08/21/91   BY: bjb *D811*/
/* REVISION: 7.0      LAST MODIFIED: 09/16/91   BY: MLV *F015**/
/* REVISION: 7.0      LAST MODIFIED: 02/02/92   BY: pml *F129**/
/* REVISION: 7.0      LAST MODIFIED: 02/12/92   BY: tjs *F202**/
/* REVISION: 7.0      LAST MODIFIED: 03/04/92   BY: tjs *F247* (rev only)*/
/* REVISION: 7.0      LAST MODIFIED: 03/17/92   BY: TMD *F259**/
/* REVISION: 7.0      LAST MODIFIED: 03/20/92   BY: TMD *F263* (rev only)*/
/* REVISION: 7.0      LAST MODIFIED: 03/20/92   BY: dld *F322*           */
/* REVISION: 7.0      LAST MODIFIED: 04/08/92   BY: tmd *F367* (rev only)*/
/* REVISION: 7.0      LAST MODIFIED: 04/06/92   BY: dld *F358*           */
/* REVISION: 7.0      LAST MODIFIED: 06/19/92   BY: tmd *F458**/
/* REVISION: 7.3      LAST MODIFIED: 09/06/92   BY: afs *G047**/
/* REVISION: 7.4      LAST MODIFIED: 08/19/93   BY: pcd *H009* */
/* REVISION: 7.4      LAST MODIFIED: 05/31/94   BY: dpm *GK02* */
/* REVISION: 8.5      LAST MODIFIED: 07/27/95   BY: taf *J053* */
/* REVISION: 8.5      LAST MODIFIED: 04/09/96   BY: jzw *G1P6* */
/* REVISION: 8.6      LAST MODIFIED: 10/04/97   BY: ckm *K0LH* */
/* REVISION: 8.6E     LAST MODIFIED: 02/23/98   BY: *L007* A. Rahane */
/* REVISION: 8.6E     LAST MODIFIED: 10/04/98   BY: *J314* Alfred Tan       */
/* REVISION: 9.1      LAST MODIFIED: 03/24/00   BY: *N08T* Annasaheb Rahane */
/* REVISION: 9.1      LAST MODIFIED: 08/12/00   BY: *N0KN* myb              */
/* REVISION: 9.1      LAST MODIFIED: 10/10/00   BY: *N0W8* Mudit Mehta      */
/* Old ECO marker removed, but no ECO header exists *F0PN*                  */
/* Revision: 1.8.2.4     BY: Katie Hilbert   DATE: 04/01/01 ECO: *P002* */
/* Revision: 1.8.2.8     BY: Karan Motwani   DATE: 10/16/02 ECO: *N1WF* */
/* Revision: 1.8.2.11    BY: Ed van de Gevel DATE: 03/05/05 ECO: *R00G* */
/* Revision: 1.8.2.12    BY: Ed van de Gevel   DATE: 07/01/05 ECO: *R01H* */
/******************************************************************************/
/* All patch markers and commented out code have been removed from the source */
/* code below. For all future modifications to this file, any code which is   */
/* no longer required should be deleted and no in-line patch markers should   */
/* be added.  The ECO marker should only be included in the Revision History. */
/******************************************************************************/
 
/* DISPLAY TITLE */
{us/mf/mfdtitle.i}
 
define new shared variable rndmthd like rnd_rnd_mthd.
define new shared variable oldcurr like ih_curr.
/*CHANGED ALL LOCAL VARIABLES TO NEW SHARED FOR soivrp09a.p */
define new shared variable cust like ih_cust.
define new shared variable cust1 like ih_cust.
define new shared variable inv like ih_inv_nbr.
define new shared variable inv1 like ih_inv_nbr.
define new shared variable nbr like ih_nbr.
define new shared variable nbr1 like ih_nbr.
define new shared variable name like ad_name.
define new shared variable spsn like sp_addr.
define new shared variable spsn1 like spsn.
define new shared variable po like ih_po.
define new shared variable po1 like ih_po.
define new shared variable gr_margin like idh_price label "Unit Margin"
   format "->>>>>,>>9.99".
define new shared variable ext_price like idh_price label "Ext Price"
   format "->>,>>>,>>>.99".
define new shared variable ext_gr_margin like gr_margin
   label "Ext Margin".
define new shared variable desc1 like pt_desc1 format "x(49)".
define new shared variable curr_cost like idh_std_cost.
define new shared variable base_price like ext_price.
define new shared variable base_margin like ext_gr_margin.
define new shared variable ext_cost like idh_std_cost.
define new shared variable base_rpt like ih_curr.
define new shared variable disp_curr as character
   format "x(1)" label "C".
define new shared variable ih_recno as recid.
define new shared variable tot_trl1 like ih_trl1_amt.
define new shared variable tot_trl3 like ih_trl3_amt.
define new shared variable tot_trl2 like ih_trl2_amt.
define new shared variable tot_disc like ih_trl1_amt label "Discount".
define new shared variable rpt_tot_tax like ih_trl2_amt
   label "Total Tax".
define new shared variable tot_ord_amt like ih_trl3_amt label "Total".
define new shared variable net_price like idh_price.
define new shared variable base_net_price like net_price
                                 format "->>>>,>>>,>>9.99".
define new shared variable detail_lines like mfc_logical.
define new shared variable bill  like ih_bill.
define new shared variable bill1 like ih_bill.
define new shared variable ship  like ih_ship.
define new shared variable ship1 like ih_ship.
define new shared variable site  like ih_site.
define new shared variable site1 like ih_site.
define variable maint like mfc_logical.
 
define new shared variable prt_cor like mfc_logical no-undo
   label "Correction Invoices" initial no.
   
 
find first soc_ctrl no-lock  where soc_domain = global_domain no-error.
 
{us/so/soivtot1.i "NEW" }  /* Define variables for invoice totals. */
 
maint = no.
 
form
   inv            colon 15
   inv1           label "To" colon 49 skip
   nbr            colon 15
   nbr1           label "To" colon 49 skip
   cust           colon 15
   cust1          label "To" colon 49 skip
   bill           colon 15
   bill1          label "To" colon 49 skip
   ship           colon 15
   ship1          label "To" colon 49 skip
   site           colon 15
   site1          label "To" colon 49 skip
   spsn           colon 15
   spsn1          label "To" colon 49 skip
   po             colon 15
   po1            label "To" colon 49
   base_rpt       colon 15 skip
   space(1)
   prt_cor                 skip
with frame a side-labels width 80.
prt_cor:hidden in frame a = (soc_use_corr_inv = no).
 
/* SET EXTERNAL LABELS */
setFrameLabels(frame a:handle).
  
repeat:
   assign
      oldcurr = ""
      tot_trl1     = 0
      tot_trl2     = 0
      tot_trl3     = 0
      tot_disc     = 0
      rpt_tot_tax  = 0
      tot_ord_amt  = 0.
 
   if inv1 = hi_char then inv1 = "".
   if nbr1 = hi_char then nbr1 = "".
   if cust1 = hi_char then cust1 = "".
   if bill1 = hi_char then bill1 = "".
   if ship1 = hi_char then ship1 = "".
   if site1 = hi_char then site1 = "".
   if spsn1 = hi_char then spsn1 = "".
   if po1 = hi_char then po1 = "".
 
   update inv inv1 nbr nbr1 cust cust1
      bill bill1
      ship ship1
      site site1
      spsn spsn1 po po1
      base_rpt
      prt_cor when (soc_use_corr_inv)
   with frame a.
  
   bcdparm = "".
   {us/mf/mfquoter.i inv     }
   {us/mf/mfquoter.i inv1    }
   {us/mf/mfquoter.i nbr     }
   {us/mf/mfquoter.i nbr1    }
   {us/mf/mfquoter.i cust    }
   {us/mf/mfquoter.i cust1   }
   {us/mf/mfquoter.i bill    }
   {us/mf/mfquoter.i bill1   }
   {us/mf/mfquoter.i ship    }
   {us/mf/mfquoter.i ship1   }
   {us/mf/mfquoter.i site    }
   {us/mf/mfquoter.i site1   }
   {us/mf/mfquoter.i spsn    }
   {us/mf/mfquoter.i spsn1   }
   {us/mf/mfquoter.i po      }
   {us/mf/mfquoter.i po1     }
   {us/mf/mfquoter.i base_rpt }
   {us/mf/mfquoter.i prt_cor}
 
   if inv1 = "" then inv1 = hi_char.
   if nbr1 = "" then nbr1 = hi_char.
   if cust1 = "" then cust1 = hi_char.
   if bill1 = "" then bill1 = hi_char.
   if ship1 = "" then ship1 = hi_char.
   if site1 = "" then site1 = hi_char.
   if spsn1 = "" then spsn1 = hi_char.
   if po1 = "" then po1 = hi_char.
  
   /* OUTPUT DESTINATION SELECTION */
   {us/gp/gpselout.i &printType = "printer"
                     &printWidth = 132
                     &pagedFlag = " "
                     &stream = " "
                     &appendToFile = " "
                     &streamedOutputToTerminal = " "
                     &withBatchOption = "yes"
                     &displayStatementType = 1
                     &withCancelMessage = "yes"
                     &pageBottomMargin = 6
                     &withEmail = "yes"
                     &withWinprint = "yes"
                     &defineVariables = "yes"}
 
   {us/bbi/mfphead.i}
/*DC45*  {us/bbi/gprun.i ""soivrp9a.p""} */
/*DC45* run /tmp/dco/us/xx/xxsoivrf9a.p. */

/*DC45*/  {us/bbi/gprun.i ""xxsoivrf9a.p""} 


   /* REPORT TRAILER */
   {us/mf/mfrtrail.i}
 
end.
