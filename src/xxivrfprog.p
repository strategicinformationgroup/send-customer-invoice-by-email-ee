/* Author      : Dumitru Cotfas
*/
/* Date Created: 18-Apr-2017                                                 */
/* Call RFW - urn:qad-report:c1:xxxxxxxxxxxxxxxxxxxxxxx                       */
/* Modification History                                                       */
/****************************************************************************/

{us/bbi/mfdeclre.i}

define shared variable l_batch             as   character       no-undo.
define shared variable l_email             as   character       no-undo.
define shared variable l_submit            as   character       no-undo.
define shared variable l_inv_nbr            as   character       no-undo.
define shared variable l_comp_addr         as   character       no-undo.

define shared variable email-addresses     as   character       no-undo.
define shared variable email-subject       as   character       no-undo.
define shared variable email-body          as   character       no-undo.
define shared variable v_sys_ips           as   character       no-undo.

define variable v_subj_body          as   character       no-undo.

define shared variable v_report-id          as   integer         no-undo.
define shared variable v_error-code         as   integer         no-undo.

define variable v_type as character no-undo.
define variable v_sig_test as character.


find first soc_ctrl where soc_domain = global_domain no-lock no-error.
l_comp_addr = soc_comp.

run p-submit-qrfrpt (input l_inv_nbr, input l_email, input l_batch).

FUNCTION f_get_code_mstr returns character
   (input fv_domain as CHARACTER, input fv_fld as character, input fv_value as character) forward.   


{us/xx/xxqrffunc.i}  


procedure p-submit-qrfrpt :

    define input parameter so-nbr       like ih_nbr.
    define input parameter in-email     like code_cmmt.
    define input parameter in-batch     as character.

    define variable report-id           as   integer.
    define variable error-code          as   integer.

    v_subj_body = "[SUBJECT]" + chr(13)
            + email-subject    + chr(13)
            + "[BODY]"    + chr(13)
            + email-body    + chr(13) + chr(13) + chr(13).

    xqrf-init-batch("SIG_InvoiceReprintForm", in-batch). 
      
    xqrf-parameter("tt_ih_hist.ih_inv_nbr" , "="  , l_inv_nbr ,  "").
xqrf-parameter("ttreportoptions.show_serial" , "="  ,  "true"  ,  "").
xqrf-parameter("ttreportoptions.show_comments" , "="  ,  "true"  ,  "").
xqrf-parameter("ttreportoptions.comp_addr" , "="  ,  "10"  ,  "").
xqrf-parameter("ttreportoptions.print_language" , "="  ,  "us"  ,  "").
/*xqrf-parameter("sys_default_report_definition" , "="  ,  "SIG_InvoiceReprintForm"  ,  "").*/
/*xqrf-parameter("sys_render_as" , "="  ,  "3"  ,  "").*/
/*xqrf-parameter("sys_domain" , "="  ,  "USUSD10"  ,  "").*/
/*xqrf-parameter("sys_entity" , "="  ,  "100"  ,  "").*/
/*xqrf-parameter("sys_trusted_signon_session" , "="  ,  "dcotfas"  ,  "").*/
xqrf-parameter("sys_ips" , "="  ,  v_sys_ips  ,  "").
xqrf-parameter("sys_language" , "="  ,  "en-US"  ,  "").
/*xqrf-parameter("sys_mfg_language" , "="  ,  "us"  ,  "").*/
/*xqrf-parameter("sys_search_criteria_display" , "="  ,  "1"  ,  "").*/
xqrf-parameter("sys_ci_short_date_pattern" , "="  ,  "M/d/yyyy"  ,  "").
/*xqrf-parameter("sys_ci_date_separator" , "="  ,  "/"  ,  "").*/
/*xqrf-parameter("sys_ci_decimal_separator" , "="  ,  "."  ,  "").*/
/*xqrf-parameter("sys_ci_decimal_digits" , "="  ,  "2"  ,  "").*/
/*xqrf-parameter("sys_ci_group_separator" , "="  ,  ","  ,  "").*/
/*xqrf-parameter("sys_ci_group_sizes" , "="  ,  "3"  ,  "").*/
/*xqrf-parameter("sys_base_currency" , "="  ,  "USD"  ,  "").*/
xqrf-parameter("sys_run_as_burst" , "="  ,  "False"  ,  "").
xqrf-parameter("sys_is_burst_driver_report" , "="  ,  "False"  ,  "").
xqrf-parameter("sys_burst_in_process" , "="  ,  "False"  ,  "").
xqrf-parameter("sys_burst_description" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_driver_report_email" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_driver_report_inbox" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_target_report_email" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_target_report_inbox" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_target_report_printer" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_target_report_save_output" , "="  ,  "True"  ,  "").
xqrf-parameter("sys_burst_target_report_link_to_email" , "="  ,  "True"  ,  "").
xqrf-parameter("sys_burst_target_report_attach_to_email" , "="  ,  "False"  ,  "").
xqrf-parameter("sys_burst_parameter_repository" , "="  ,  ""  ,  "").
xqrf-parameter("sys_burst_split_by_field_name" , "="  ,  ""  ,  "").
xqrf-parameter("sys_inbox" , "="  ,  ""  ,  "QAD_DBNULL").


    xqrf-parameter("sys_email" , "="  ,  l_email  ,  "QAD_DBNULL"). 
xqrf-parameter("sys_save_output" , "="  ,  "True"  ,  "QAD_DBNULL").
/*xqrf-parameter("sys_attach_to_email" , "="  ,  "True"  ,  "QAD_DBNULL").*/
/*xqrf-parameter("sys_link_to_email" , "="  ,  "True"  ,  "QAD_DBNULL").*/
    
/*xqrf-parameter("sys_email_template1" , "="  ,  v_subj_body  ,  "QAD_DBNULL"). */
    
    xqrf-description("Invoice_Print -- " + l_inv_nbr).
/*
    xqrf-output-path("invoice_print/").
    xqrf-output-file(l_inv_nbr).


    xqrf-email-info(in-email, email-subject, email-body).
*/

    xqrf-submit(output report-id, output error-code, l_submit).

    v_report-id   = report-id.
    v_error-code  = error-code.
    
end procedure.      /* p-submit-invoice */



 FUNCTION f_get_code_mstr returns character
   (input fv_domain as CHARACTER, input fv_fld as character, input fv_value as character):

   def var flv_char as character no-undo.

   for each code_mstr no-lock
   where code_domain = fv_domain
   and code_fldname = fv_fld
   and code_value begins fv_value:
      flv_char = flv_char + trim(code_cmmt).
   end.

   return flv_char.

END FUNCTION.